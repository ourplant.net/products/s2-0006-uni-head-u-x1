The S2-0006 UNI-HEAD U-X1 has the following technical specifications.

## Technical information
| Weight                    |            5.2 kg |
|:----|----:|
| Dimensions                | 79 × 205 × 514 mm |
| Movement range in Z in mm |               150 |
| Voltage in V              |                24 |
| Max. current in A         |                 4 |
| Communication interface   |UNICAN|


